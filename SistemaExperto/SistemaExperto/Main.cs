﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaExperto
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void btn_reglas_Click(object sender, EventArgs e)
        {
            //Iniciemos El form de reglas, Nos vemos ahi :D
             new Reglas().Show();
        }

        private void btn_atomo_Click(object sender, EventArgs e)
        {
            //Iniciemos El form de atomos, Nos vemos ahi :D
            new a().Show();
        }

        private void btn_begin_Click(object sender, EventArgs e)
        {
            //Iniciemos El form de preguntas, Nos vemos ahi, no mames, ya hazlo y ya
            new Preguntas().Show();
        }
    }
}
