﻿namespace SistemaExperto
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_reglas = new System.Windows.Forms.Button();
            this.btn_atomo = new System.Windows.Forms.Button();
            this.btn_begin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_reglas
            // 
            this.btn_reglas.Location = new System.Drawing.Point(12, 12);
            this.btn_reglas.Name = "btn_reglas";
            this.btn_reglas.Size = new System.Drawing.Size(75, 23);
            this.btn_reglas.TabIndex = 0;
            this.btn_reglas.Text = "Reglas";
            this.btn_reglas.UseVisualStyleBackColor = true;
            this.btn_reglas.Click += new System.EventHandler(this.btn_reglas_Click);
            // 
            // btn_atomo
            // 
            this.btn_atomo.Location = new System.Drawing.Point(142, 12);
            this.btn_atomo.Name = "btn_atomo";
            this.btn_atomo.Size = new System.Drawing.Size(75, 23);
            this.btn_atomo.TabIndex = 1;
            this.btn_atomo.Text = "Atomos";
            this.btn_atomo.UseVisualStyleBackColor = true;
            this.btn_atomo.Click += new System.EventHandler(this.btn_atomo_Click);
            // 
            // btn_begin
            // 
            this.btn_begin.Location = new System.Drawing.Point(76, 47);
            this.btn_begin.Name = "btn_begin";
            this.btn_begin.Size = new System.Drawing.Size(75, 23);
            this.btn_begin.TabIndex = 2;
            this.btn_begin.Text = "Iniciar";
            this.btn_begin.UseVisualStyleBackColor = true;
            this.btn_begin.Click += new System.EventHandler(this.btn_begin_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 82);
            this.Controls.Add(this.btn_begin);
            this.Controls.Add(this.btn_atomo);
            this.Controls.Add(this.btn_reglas);
            this.Name = "Main";
            this.Text = "Experto";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_reglas;
        private System.Windows.Forms.Button btn_atomo;
        private System.Windows.Forms.Button btn_begin;
    }
}

