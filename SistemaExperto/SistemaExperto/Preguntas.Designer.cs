﻿namespace SistemaExperto
{
    partial class Preguntas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.BtnRadioY = new System.Windows.Forms.RadioButton();
            this.BtnRadioN = new System.Windows.Forms.RadioButton();
            this.BtnNext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "¿Pregunta?";
            // 
            // BtnRadioY
            // 
            this.BtnRadioY.AutoSize = true;
            this.BtnRadioY.Location = new System.Drawing.Point(12, 125);
            this.BtnRadioY.Name = "BtnRadioY";
            this.BtnRadioY.Size = new System.Drawing.Size(34, 17);
            this.BtnRadioY.TabIndex = 1;
            this.BtnRadioY.TabStop = true;
            this.BtnRadioY.Text = "Si";
            this.BtnRadioY.UseVisualStyleBackColor = true;
            // 
            // BtnRadioN
            // 
            this.BtnRadioN.AutoSize = true;
            this.BtnRadioN.Location = new System.Drawing.Point(12, 148);
            this.BtnRadioN.Name = "BtnRadioN";
            this.BtnRadioN.Size = new System.Drawing.Size(39, 17);
            this.BtnRadioN.TabIndex = 2;
            this.BtnRadioN.TabStop = true;
            this.BtnRadioN.Text = "No";
            this.BtnRadioN.UseVisualStyleBackColor = true;
            // 
            // BtnNext
            // 
            this.BtnNext.Location = new System.Drawing.Point(126, 189);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Size = new System.Drawing.Size(75, 23);
            this.BtnNext.TabIndex = 3;
            this.BtnNext.Text = "Siguiente";
            this.BtnNext.UseVisualStyleBackColor = true;
            // 
            // Preguntas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(213, 224);
            this.Controls.Add(this.BtnNext);
            this.Controls.Add(this.BtnRadioN);
            this.Controls.Add(this.BtnRadioY);
            this.Controls.Add(this.label1);
            this.Name = "Preguntas";
            this.Text = "Preguntas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton BtnRadioY;
        private System.Windows.Forms.RadioButton BtnRadioN;
        private System.Windows.Forms.Button BtnNext;
    }
}