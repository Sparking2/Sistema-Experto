﻿namespace SistemaExperto
{
    partial class a
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_newAtom = new System.Windows.Forms.Button();
            this.txtAtomo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.systemDataBaseDataSet = new SistemaExperto.SystemDataBaseDataSet();
            this.dICCIONARIOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dICCIONARIOTableAdapter = new SistemaExperto.SystemDataBaseDataSetTableAdapters.DICCIONARIOTableAdapter();
            this.tableAdapterManager = new SistemaExperto.SystemDataBaseDataSetTableAdapters.TableAdapterManager();
            ((System.ComponentModel.ISupportInitialize)(this.systemDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dICCIONARIOBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Agregar Nuevo Atomo:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btn_newAtom
            // 
            this.btn_newAtom.Location = new System.Drawing.Point(124, 57);
            this.btn_newAtom.Name = "btn_newAtom";
            this.btn_newAtom.Size = new System.Drawing.Size(101, 28);
            this.btn_newAtom.TabIndex = 1;
            this.btn_newAtom.Text = "Agregar Nuevo";
            this.btn_newAtom.UseVisualStyleBackColor = true;
            // 
            // txtAtomo
            // 
            this.txtAtomo.Location = new System.Drawing.Point(15, 25);
            this.txtAtomo.Name = "txtAtomo";
            this.txtAtomo.Size = new System.Drawing.Size(210, 20);
            this.txtAtomo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Lista de Atomos Actuales:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.Location = new System.Drawing.Point(15, 124);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBox1.Size = new System.Drawing.Size(210, 110);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // systemDataBaseDataSet
            // 
            this.systemDataBaseDataSet.DataSetName = "SystemDataBaseDataSet";
            this.systemDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dICCIONARIOBindingSource
            // 
            this.dICCIONARIOBindingSource.DataMember = "DICCIONARIO";
            this.dICCIONARIOBindingSource.DataSource = this.systemDataBaseDataSet;
            // 
            // dICCIONARIOTableAdapter
            // 
            this.dICCIONARIOTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DICCIONARIOTableAdapter = this.dICCIONARIOTableAdapter;
            this.tableAdapterManager.REGLASTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = SistemaExperto.SystemDataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // a
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(245, 254);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAtomo);
            this.Controls.Add(this.btn_newAtom);
            this.Controls.Add(this.label1);
            this.Name = "a";
            this.Text = "Atomos";
            this.Load += new System.EventHandler(this.a_Load);
            ((System.ComponentModel.ISupportInitialize)(this.systemDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dICCIONARIOBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_newAtom;
        private System.Windows.Forms.TextBox txtAtomo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private SystemDataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private SystemDataBaseDataSetTableAdapters.DICCIONARIOTableAdapter dICCIONARIOTableAdapter;
        private System.Windows.Forms.BindingSource dICCIONARIOBindingSource;
        private SystemDataBaseDataSet systemDataBaseDataSet;
    }
}